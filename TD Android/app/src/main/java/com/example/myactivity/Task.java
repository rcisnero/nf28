package com.example.myactivity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Task implements Parcelable {
    private String taskName;
    private String status;
    private String priority;
    private Date deadline;

    /** Construvteur classique de notre classe Task */
    public Task(String taskName, String status, String priority, Date deadline) {
        this.taskName = taskName;
        this.status = status;
        this.priority = priority;
        this.deadline = deadline;
    }

    /** Constructeur à travers d'un Parcel */
    public Task(Parcel in) {
        this.taskName = in.readString();
        this.status = in.readString();
        this.priority = in.readString();
        this.deadline = new Date(in.readLong());
    }

    /** Getters */
    public String getTaskName() {
        return taskName;
    }
    public String getStatus() {
        return status;
    }
    public String getPriority() {
        return priority;
    }
    public Date getDeadline() {
        return deadline;
    }

    /** Implementation des méthodes Parcelable */
    @Override
    public int describeContents() {
        return 0;
    }

    /** Dans les méthodes write et read, on fait attention à que l'ordre soit le même
     * (dans ce cas : 1. libellé, 2. status, 3. priorité, 4. deadline) */
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(taskName);
        parcel.writeString(status);
        parcel.writeString(priority);
        parcel.writeLong(deadline.getTime());
    }
    public void readFromParcel(Parcel in) {
        taskName = in.readString();
        status = in.readString();
        priority = in.readString();
        deadline = new Date(in.readLong());
    }

    /** Création d'un objet CREATOR permettant de créer une instance
     * de l’objet Task depuis un Parcel (conversion Parcel vers l’objet) */
    public static final Parcelable.Creator<Task> CREATOR = new Parcelable.Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel source){ return new Task(source); }

        @Override
        public Task[] newArray(int size){ return new Task[size]; }
    };

}
