package com.example.myactivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public final static int IDENTIFICATION = 100; /** identifiant de la requête */
    ArrayList<Task> arrayList; /** Arraylist de toutes les tâches (à adapter avec notre CustomTaskAdapter) */
    CustomTaskAdapter taskAdapter; /** Adapter dérivé de la classe ArrayAdapter */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /** Initialisation de l'arrayList, de l'adapter dérivée, du bouton "ajouter" et de la listView*/
        arrayList = new ArrayList<>();
        taskAdapter = new CustomTaskAdapter(this, arrayList);
        ListView listView1 = (ListView) findViewById(R.id.listview1);
        FloatingActionButton b = (FloatingActionButton) findViewById(R.id.button1);
        b.setOnClickListener(view -> {
            Intent addTask = new Intent(MainActivity.this, AddTask.class);
            startActivityForResult(addTask, IDENTIFICATION);
        });

        /** Liaison listView <-> adapter dérivé */
        listView1.setAdapter(taskAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IDENTIFICATION) {
            /** Récuperation de l'objet task par l'activité 2 et ajout dans l'array */
            Task task = (Task) data.getParcelableExtra("task");
            if (task != null) {
                arrayList.add(task);
                taskAdapter.notifyDataSetChanged();
            }
        }
    }
}