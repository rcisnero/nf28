package com.example.myactivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AddTask extends AppCompatActivity {
    private EditText txtTaskName;
    private RadioGroup rdgStatut;
    private RadioGroup rdgPriority;
    private RadioButton rdbStatut;
    private RadioButton rdbPriority;
    private DatePicker datePicker;
    private Button btnCancel;
    private Button btnClear;
    private Button btnOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /** Création de l'activité, recupération du layout et
         * recherche des widgets par son ID */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        txtTaskName = (EditText) findViewById(R.id.txtTaskName);
        rdgStatut = (RadioGroup) findViewById(R.id.rdgStatut);
        rdgPriority = (RadioGroup) findViewById(R.id.rdgPriority);
        datePicker = (DatePicker) findViewById(R.id.datePicker);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnClear = (Button) findViewById(R.id.btnClear);
        btnOk = (Button) findViewById(R.id.btnOk);

        /** Listeners des trois boutons */
        btnCancel.setOnClickListener(view -> {
            Toast.makeText(getApplicationContext(), "Opération annulée", Toast.LENGTH_LONG).show();
            Intent onClickCancel = new Intent();
            setResult(200, onClickCancel);
            finish();
        });
        btnClear.setOnClickListener(view -> {
            Toast.makeText(getApplicationContext(), "Champs réinitialisés", Toast.LENGTH_LONG).show();
            txtTaskName.setText("");
        });
        btnOk.setOnClickListener(view -> {
            Toast.makeText(getApplicationContext(), "Tâche ajoutée", Toast.LENGTH_LONG).show();

            /** Récupération du libellé de la tâche */
            String taskName = txtTaskName.getText().toString();

            /** Récupération du statut */
            int idStatut = rdgStatut.getCheckedRadioButtonId();
            rdbStatut = (RadioButton) findViewById(idStatut);

            /** Récupération de la priorité */
            int idPriority = rdgPriority.getCheckedRadioButtonId();
            rdbPriority = (RadioButton) findViewById(idPriority);

            /** Récupération de la date */
            int day = datePicker.getDayOfMonth();
            int month = datePicker.getMonth();
            int year = datePicker.getYear();
            Calendar calendar = new GregorianCalendar(year, month, day);
            Date newDate = calendar.getTime();

            /** Création de l'objet Task et on l'envoie à la activité principale par moyen d'un intent
             * avec recuperation du resultat (ici, on envoie le code 100) */
            Task newTask = new Task(taskName, rdbStatut.getText().toString(), rdbPriority.getText().toString(), newDate);
            Intent onClickOk = new Intent();
            onClickOk.putExtra("task", newTask);
            setResult(100, onClickOk);
            finish();
        });
    }
}