package com.example.myactivity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Cette classe est derivée de ArrayAdapter pour implementer le layout adapté
 * pour la listView,
 */
public class CustomTaskAdapter extends ArrayAdapter<Task> {
    private Context context;
    private ArrayList<Task> items;

    public CustomTaskAdapter(Context context, ArrayList<Task> items) {
        super(context, 0, items);
        this.context = context;
        this.items = items;
    }

    /** Implémentation de la méthode getView, ici on appelle le layout task_item.xml,
     * puis on récupère les widgets et on affiche l'information obtenue par AddTask */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.task_item, parent, false);
        }

        /** On récupère la tâche courant */
        Task currentTask = (Task) getItem(position);

        /** Recherche des widgets par son ID */
        TextView txtTaskName = (TextView) convertView.findViewById(R.id.taskName);
        TextView txtTaskStatus = (TextView) convertView.findViewById(R.id.taskStatus);
        TextView txtTaskPriority = (TextView) convertView.findViewById(R.id.taskPriority);
        TextView txtTaskDeadline = (TextView) convertView.findViewById(R.id.taskDeadline);

        /** Affichage des attributs de la tâche */
        if (currentTask != null) {
            txtTaskName.setText(currentTask.getTaskName());
            txtTaskStatus.setText("Status : " + currentTask.getStatus());
            txtTaskPriority.setText("Priorité : " + currentTask.getPriority());
            txtTaskDeadline.setText("Deadline : " +
                    new SimpleDateFormat("dd-MM-yyyy").format(currentTask.getDeadline()));
        }

        return convertView;
    }
}
