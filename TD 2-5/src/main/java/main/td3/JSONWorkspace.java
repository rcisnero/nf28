package main.td3;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.collections.ObservableList;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class JSONWorkspace {

    public void setGroups(ArrayList<Group> groupArrayList, ObservableList<Group> groupObservableList) {
        Map groupMap;
        Map contactMap;
        for (int i = 0; i < groupArrayList.size(); i++) {
            groupMap = (Map) groupArrayList.get(i);
            groupObservableList.add(new Group((String) groupMap.get("groupName")));
            ArrayList<Contact> contacts = (ArrayList<Contact>) groupMap.get("contactList");
            for (int j = 0; j < contacts.size(); j++) {
                contactMap = (Map) contacts.get(j);
                Contact newContact = new Contact();
                newContact.setFirstName((String) contactMap.get("firstName"));
                newContact.setLastName((String) contactMap.get("lastName"));
                newContact.setCity((String) contactMap.get("city"));
                newContact.setCountry((String) contactMap.get("country"));
                newContact.setSex((String) contactMap.get("sex"));
                newContact.setValidData((boolean) contactMap.get("validData"));
                groupObservableList.get(i).addContact(newContact);
            }
        }
    }

    public ArrayList<Group> fromFile(File file) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ArrayList<Group> groupList;
        groupList = mapper.readValue(new FileReader(file), ArrayList.class);
        return groupList;
    }

    public void save(File file, ObservableList<Group> groupList)  throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(file, groupList);
    }
}
