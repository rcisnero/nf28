package main.td3;

import javafx.collections.MapChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ContactController implements Initializable {
    @FXML
    private TextField txtFirstName;
    @FXML
    private TextField txtLastName;
    @FXML
    private TextField txtCity;
    @FXML
    private ComboBox<String> cmbCountry;
    @FXML
    private RadioButton rbtFemale;
    @FXML
    private RadioButton rbtMale;
    @FXML
    private ToggleGroup sexGroup;
    @FXML
    protected Button valider;

    public Contact editingContact;
    private Map<String, Control> controlMap;
    MapChangeListener<String, String> validateFunction;

    public ContactController() {
        editingContact = new Contact();
        controlMap = new LinkedHashMap<>();
    }

    public ComboBox<String> getCmbCountry() { return cmbCountry; }
    public RadioButton getRbtFemale() { return rbtFemale; }
    public RadioButton getRbtMale() { return rbtMale; }

    private void setCountries(){
        cmbCountry.getItems().setAll(Country.getCountryNames());
    }

    private void setBinding(){
        txtFirstName.textProperty().bindBidirectional(editingContact.firstNameProperty());
        txtLastName.textProperty().bindBidirectional(editingContact.lastNameProperty());
        txtCity.textProperty().bindBidirectional(editingContact.cityProperty());

        cmbCountry.getSelectionModel().selectedItemProperty().addListener((o, oldValue, newValue) ->
                editingContact.countryProperty().setValue(newValue)
        );
        sexGroup.selectedToggleProperty().addListener((o, oldValue, newValue) -> {
            if (((RadioButton)newValue).getText().equals("M")) {
                editingContact.setSex("Male");
            }
            else if (((RadioButton)newValue).getText().equals("F")) {
                editingContact.setSex("Female");
            }
        });
    }

    private void setValidateFunction() {
        validateFunction = change -> {
            Control control = controlMap.get(change.getKey());
            if (change.wasAdded()) {
                control.setStyle("-fx-border-color: red ;");
                control.setTooltip(new Tooltip(change.getValueAdded()));
            }
            else if (change.wasRemoved()) {
                control.setStyle("-fx-border-color:  blue ;");
                control.setTooltip(null);
            }
        };
        editingContact.messageValidationProperty().addListener(validateFunction);
    }

    private void addControls(){
        controlMap.put(editingContact.firstNameProperty().getName(), txtFirstName);
        controlMap.put(editingContact.lastNameProperty().getName(), txtLastName);
        controlMap.put(editingContact.cityProperty().getName(), txtCity);
        controlMap.put(editingContact.countryProperty().getName(), cmbCountry);

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setCountries();
        setBinding();
        setValidateFunction();
        addControls();
    }

    public boolean onClickValidate() { return editingContact.validate(); }
}