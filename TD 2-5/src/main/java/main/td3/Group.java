package main.td3;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;

public class Group {
    private StringProperty groupName;
    private ObservableList<Contact> contactList;

    /** Constructeurs */
    public Group() {
        groupName = new SimpleStringProperty(null, "groupName", "");
        contactList = FXCollections.observableArrayList();
    }
    public Group(String name) {
        groupName = new SimpleStringProperty(null, "groupName", name);
        contactList = FXCollections.observableArrayList();
    }

    /** Accesseurs en lecture */
    public String getGroupName() {
        return groupName.get();
    }
    public StringProperty groupNameProperty() {
        return groupName;
    }
    public ObservableList<Contact> getContactList() { return contactList; }

    /** Accesseurs en écriture */
    public void setGroupName(String groupName) {
        this.groupName.set(groupName);
    }
    public void setContactList(ObservableList<Contact> contactList) {
        this.contactList = contactList;
    }

    /** Methodes utiles pour le controleur */
    public void addContact(Contact c) { contactList.add(c); }
    public void removeContact(Contact c) { contactList.remove(c); }

    /** Surcharge de la methode toString */
    @Override
    public String toString() { return groupName.getValue(); }

}
