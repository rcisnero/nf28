package main.td3;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class TD3_Controller implements Initializable {
    @FXML
    private BorderPane rootPane = new BorderPane();
    @FXML
    private TreeView<Object> contactsTreeView;
    @FXML
    private Button btnAdd;
    @FXML
    private Button btnRemove;
    @FXML
    private MenuItem menuOuvrir;
    @FXML
    private MenuItem menuEnregistrer;

    Parent contactPane;
    Contact editingContact;
    Contact originalContact;
    ContactController contactController;
    boolean editMode = false;
    protected ObservableList<Group> groupObservableList = FXCollections.observableArrayList();
    private final Image groupIcon = new Image(getClass().getResourceAsStream("Images/group.png"));
    private final Image contactIcon = new Image(getClass().getResourceAsStream("Images/contact.png"));
    JSONWorkspace jsonWorkspace = new JSONWorkspace();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /** Recuperation du controleur, vue et modèle depuis ContactController (celui du TD2) */
        ContactControl cc = new ContactControl();
        contactController = cc.contactController;
        editingContact = contactController.editingContact;
        originalContact = new Contact();

        /** Ajout dans l'interface principal et initiation de l'interface graphique */
        contactPane = cc.root;
        rootPane.setCenter(contactPane);
        contactPane.visibleProperty().set(false);
        TreeItem<Object> ficheDeContacts = new TreeItem<>("Fiche de contacts");
        ficheDeContacts.setExpanded(true);

        /** Mise en place de la cellFactory */
        contactsTreeView.setRoot(ficheDeContacts);
        contactsTreeView.setEditable(true);
        contactsTreeView.setCellFactory(param -> new TextFieldTreeCellImpl());

        /** Repertoire pour la sauvegarde de données */
        String jsonPath = Paths.get(".").toAbsolutePath().normalize() + "/JSON_Files";
        File jsonFolder = new File(jsonPath);
        if (!jsonFolder.exists())
            jsonFolder.mkdirs();

        /** Initiation des listeners */
        contactsTreeViewListener();
        groupListListener();
        validDataListener();
        validerButtonListener();
        menuListeners(jsonFolder);
    }

    /** Listeners des boutons pour ouvrir et sauvegarder un fichier JSON */
    private void menuListeners(File jsonFolder) {
        menuOuvrir.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(jsonFolder);
            File file = fileChooser.showOpenDialog(null);
            ArrayList<Group> groupArrayList = new ArrayList<>();

            if (file != null) {
                groupObservableList.clear();
                try {
                    groupArrayList = jsonWorkspace.fromFile(file);
                    jsonWorkspace.setGroups(groupArrayList, groupObservableList);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        menuEnregistrer.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(jsonFolder);
            File file = new File(fileChooser.showSaveDialog(null) + ".json");
            if (file != null) {
                try {
                    jsonWorkspace.save(file, groupObservableList);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    /** Listener sur la property de EditingContact */
    private void validDataListener() {
        editingContact.validDataProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                Group group;
                Object g = contactsTreeView.getSelectionModel().getSelectedItem().getValue();
                if (g instanceof Contact) {
                    group = (Group) contactsTreeView.getSelectionModel().getSelectedItem().getParent().getValue();
                } else {
                    group = (Group) g;
                }
                Contact newContact = new Contact(editingContact);
                group.addContact(newContact);
            }
        });
    }

    /** Controleur du bouton valider du formulaire */
    private void validerButtonListener() {
        contactController.valider.setOnAction(e -> {
            if (contactController.onClickValidate()) {
                if (editMode) {
                    Group g = (Group) contactsTreeView.getSelectionModel().getSelectedItem().getParent().getValue();
                    g.removeContact(originalContact);
                }
                contactsTreeView.getSelectionModel().select(contactsTreeView.getRoot());
                resetEditContact();
                contactsTreeView.refresh();
            }
        });
    }

    /** Fonction qui determine le TreeItem à créer selon l'item selectionné */
    private void contactsTreeViewListener(){
        contactsTreeView.getSelectionModel().selectedItemProperty().addListener((v, oldVal, newVal) -> {
            if (newVal != null) {
                int level = 0;
                TreeItem<Object> tmp = newVal.getParent();
                while (tmp != null) {
                    tmp = tmp.getParent();
                    level++;
                }

                if (level == 0)  {
                    contactPane.visibleProperty().set(false);
                    btnAdd.setOnAction(e-> groupObservableList.add(new Group("New group")));
                    editMode = false;
                }
                else if (level == 1){
                    contactPane.visibleProperty().set(false);
                    editMode = false;

                    btnAdd.setOnAction(e-> {
                        contactPane.visibleProperty().set(true);
                        resetEditContact();
                    });
                }
                else if (level == 2){
                    contactPane.visibleProperty().set(true);
                    setContactForm();

                    btnAdd.setOnAction(e-> {
                        resetEditContact();
                        editMode = false;
                    });
                }
            }
        });
    }

    /** Listener sur la liste de groupes qui declenche l'ajout d'un TreeItem */
    private void groupListListener(){
        groupObservableList.addListener((ListChangeListener<Group>) change -> {
            while (change.next()) {
                if (change.wasAdded()) {
                    for (Group addGroup : change.getAddedSubList()) {
                        TreeItem<Object> newGroup = new TreeItem<>(addGroup, new ImageView(groupIcon));
                        contactsTreeView.getRoot().getChildren().add(newGroup);
                        contactListListener(addGroup);
                    }
                }
            }
        });
    }

    /** Chaque groupe a son listener dans sa liste de contacts */
    private void contactListListener(Group group) {
        group.getContactList().addListener((ListChangeListener<Contact>) change -> {
            while (change.next()) {
                if (change.wasAdded()) {
                    change.getAddedSubList().forEach(value -> {
                        Object selection = null;
                        try {
                            selection = contactsTreeView.getSelectionModel().getSelectedItem().getValue();
                        } catch (NullPointerException ex) {}

                        if (selection != null) {
                            if (selection instanceof Contact) {
                                contactsTreeView.getSelectionModel().getSelectedItem().getParent().getChildren().add(
                                        new TreeItem<>(value, new ImageView(contactIcon)));
                            } else {
                                contactsTreeView.getSelectionModel().getSelectedItem().getChildren().add(
                                        new TreeItem<>(value, new ImageView(contactIcon)));
                            }
                        } else {
                            TreeItem<Object> groupItem = new TreeItem<>(group);
                            contactsTreeView.getSelectionModel().select(groupItem);
                            selection = groupItem.getValue();
                        }

                        contactsTreeView.getSelectionModel().getSelectedItem().setExpanded(true);
                    });
                }
                if (change.wasRemoved()) {
                    change.getRemoved().forEach(value -> {
                        contactsTreeView.getSelectionModel().getSelectedItem().getParent().getChildren().remove(
                                contactsTreeView.getSelectionModel().getSelectedItem());
                    });
                }
            }
        });
    }

    /** Fonction qui copie les attributs d'un TreeItem <contact> dans le formulaire d'edition */
    private void setContactForm() {
        originalContact = (Contact) contactsTreeView.getSelectionModel().getSelectedItem().getValue();
        editingContact.copyContact(originalContact);
        contactController.getCmbCountry().setValue(editingContact.getCountry());
        if (editingContact.getSex().equals("Female")) {
            contactController.getRbtFemale().setSelected(true);
        } else {
            contactController.getRbtMale().setSelected(true);
        }

        editMode = true;
    }

    /** Mis à zero des attributs de @param editingContact*/
    private void resetEditContact() {
        this.editingContact.firstNameProperty().setValue("");
        this.editingContact.lastNameProperty().setValue("");
        this.editingContact.cityProperty().setValue("");
        this.editingContact.countryProperty().setValue("");
        this.contactController.getCmbCountry().getSelectionModel().clearSelection();
        this.editingContact.setSex("Female");
        this.contactController.getRbtFemale().setSelected(true);
        this.editingContact.validDataProperty().setValue(false);
    }

    /** Fonction du TD pour l'édition des noms des groupes */
    private final class TextFieldTreeCellImpl extends TreeCell<Object> {
        private TextField textField;
        public TextFieldTreeCellImpl() { }

        @Override
        public void startEdit() {
            if (!(contactsTreeView.getSelectionModel().getSelectedItem().getValue() instanceof Group))
                return;
            super.startEdit();
            if (textField == null) {
                createTextField();
            }
            setText(null);
            setGraphic(textField);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            setText(getTreeItem().getValue().toString());
            setGraphic(getTreeItem().getGraphic());
        }

        @Override
        public void updateItem(Object item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setText(null);
                    setGraphic(textField);
                } else {
                    setText(getString());
                    setGraphic(getTreeItem().getGraphic());
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setOnKeyReleased(t -> {
                if (t.getCode() == KeyCode.ENTER) {
                    ((Group)getItem()).setGroupName(textField.getText());
                    commitEdit(getItem());
                } else if (t.getCode() == KeyCode.ESCAPE) {
                    cancelEdit();
                }

            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }
}
