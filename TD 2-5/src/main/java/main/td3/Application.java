package main.td3;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * On charge la fenetre principal. Le formulaire de contact va être géré par son controleur (ContactController)
 */
public class Application extends javafx.application.Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getResource("treeview.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Formulaire de contact");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
