package main.td3;

public enum Property {
    firstName("Prénom obligatoire"),
    lastName("Nom obligatoire"),
    country("Pays obligatoire"), city("Ville obligatoire");
    private Property(String tooltip) {
        this.tooltip = tooltip;
    }
    public String tooltip;
}

