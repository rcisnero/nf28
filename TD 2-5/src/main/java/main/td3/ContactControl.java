package main.td3;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

/**
 * On charge le formulaire de contact. Il va être appellé par le controleur principal (TD3_Controller)
 */
public class ContactControl {
    public Parent root;
    public ContactController contactController;

    public ContactControl() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("contact_control.fxml"));
        try {
            root = fxmlLoader.load();
            contactController = fxmlLoader.getController();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }


}


