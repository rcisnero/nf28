package main.td3;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

import java.util.function.Predicate;

public class Contact {
    private final StringProperty firstName;
    private final StringProperty lastName;
    private final StringProperty city;
    private final StringProperty country;
    private String sex;
    private final ObservableMap<String, String> messageValidation;
    private final BooleanProperty validData;

    /** Constructeurs */
    public Contact() {
        this.firstName = new SimpleStringProperty(this, "firstName", "");
        this.lastName = new SimpleStringProperty(this, "lastName", "");
        this.city = new SimpleStringProperty(this, "city", "");
        this.country = new SimpleStringProperty(this, "country", "");
        this.sex = "Female";
        messageValidation = FXCollections.observableHashMap();
        this.validData = new SimpleBooleanProperty();
    }
    public Contact(Contact c) {
        this.firstName = new SimpleStringProperty(this, "firstName", c.getFirstName());
        this.lastName = new SimpleStringProperty(this, "lastName", c.getLastName());
        this.city = new SimpleStringProperty(this, "city", c.getCity());
        this.country = new SimpleStringProperty(this, "country", c.getCountry());
        this.sex = c.getSex();
        messageValidation = FXCollections.observableHashMap();
        this.validData = new SimpleBooleanProperty();
    }

    /** Accesseurs en lecture property */
    public StringProperty firstNameProperty() { return firstName; }
    public StringProperty lastNameProperty() { return lastName; }
    public StringProperty cityProperty() { return city; }
    public StringProperty countryProperty() { return country; }
    public ObservableMap<String, String> messageValidationProperty() { return messageValidation; }
    public BooleanProperty validDataProperty() { return validData; }

    /** Accesseurs en lecture primitifs */
    public String getFirstName() { return firstName.get(); }
    public String getLastName() { return lastName.get(); }
    public String getCity() { return city.get(); }
    public String getCountry() { return country.get(); }
    public String getSex() { return sex; }
    public ObservableMap<String, String> getMessageValidation() { return messageValidation; }
    public boolean isValidData() { return validData.get(); }
    public Predicate<StringProperty> getTestProperty() { return testProperty; }

    /** Accesseurs en écriture */
    public void setFirstName(String firstName) { this.firstName.set(firstName); }
    public void setLastName(String lastName) { this.lastName.set(lastName); }
    public void setCity(String city) { this.city.set(city); }
    public void setCountry(String country) { this.country.set(country); }
    public void setSex(String sex) { this.sex = sex; }
    public void setValidData(boolean validData) { this.validData.set(validData); }
    public void setTestProperty(Predicate<StringProperty> testProperty) { this.testProperty = testProperty; }

    /** Testing et validation */
    Predicate<StringProperty> testProperty = prop -> prop.get() == null || prop.get().trim().equals("");

    /**
     * Fonction interne. Permet de valider chaque property de la classe Contact
     * @param sprop la property à vérifier
     */
    private void validate(StringProperty sprop){
        if (testProperty.test(sprop))
            messageValidation.put(sprop.getName(), Property.valueOf(sprop.getName()).tooltip);
    }

    /**
     * Peut être appellé depuis une autre classe. Permet de valider les données dans le formulaire de contact
     * @return vrai si les données son valides (càd, l'ObservableMap messageValidation est vide)
     */
    public boolean validate() {
        messageValidation.clear();
        validate(firstName);
        validate(lastName);
        validate(city);
        validate(country);
        setValidData(messageValidation.isEmpty());
        return messageValidation.isEmpty();
    }

    /**
     * Cette fonction copie le contenu d'un contact envoyé en parametre dans l'objet qui l'appelle
     * @param c un contact à copier
     */
    public void copyContact(Contact c) {
        this.setFirstName(c.getFirstName());
        this.setLastName(c.getLastName());
        this.setCity(c.getCity());
        this.setCountry(c.getCountry());
        this.setSex(c.getSex());
    }

    /**
     * Les TreeItem prennent cette methode pour obtenir le nom à afficher
     */
    @Override
    public String toString() {
        return lastName.getValue() + " " + firstName.getValue();
    }
}
