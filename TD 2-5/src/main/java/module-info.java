module main.td3 {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.fasterxml.jackson.databind;


    opens main.td3 to javafx.fxml;
    exports main.td3;
}